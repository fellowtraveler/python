import re

def parseparam(regex, list):
    with open('file.txt', 'r') as f:
        for line in f:
            result = re.search(regex, line)
            if result:
                list.append(int(result[0]))
    return list

tregex = "(?<=TIME:)\d+"
tarray = [ ]
tarray = parseparam(tregex, tarray)
atregex = "(?<=AT:)\d+"
atarray = [ ]
atarray = parseparam(atregex, atarray)

b411 = [ ]

print("time:", sum(tarray) / len(tarray))
print("Average Air Temperature:", sum(atarray) / len(atarray))
length = len(atarray)
i = 0
while i < length:
   if tarray[i] < 1100:
        b411.append(atarray[i])
   i += 1

print("Average Air Temperature before 11:", sum(b411) / len(b411))
